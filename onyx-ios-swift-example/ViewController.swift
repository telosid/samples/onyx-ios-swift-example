//
//  ViewController.swift
//  onyx-ios-swift-example
//
//  Created by Christopher Wheatley on 5/2/21.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configOnyx()
    }

    func configOnyx(){
      let onyxConfig = OnyxConfiguration()

      onyxConfig.licenseKey = "your license key here"
      onyxConfig.showLoadingSpinner = true
      onyxConfig.returnRawImage = false
      onyxConfig.returnProcessedImage = true
      onyxConfig.returnWsq = false
      onyxConfig.useOnyxLive = false
      onyxConfig.returnFingerprintTemplate = INNOVATRICS
      onyxConfig.reticleOrientation = ReticleOrientation(LEFT.rawValue)
      onyxConfig.computeNfiqMetrics = true
      onyxConfig.showLoadingSpinner = true
      onyxConfig.returnSlapImage = false
      onyxConfig.shouldBinarizeProcessedImage = false
      onyxConfig.returnFullFrameImage = false
      onyxConfig.useManualCapture = false
      onyxConfig.manualCaptureText = "Tap to manual capture"
      onyxConfig.captureFingersText = "Hold fingers steady"
      onyxConfig.captureThumbText = "Hold thumb steady"
      onyxConfig.fingersNotInFocusText = "Move fingers until in focus"
      onyxConfig.thumbNotInFocusText = "Move thumb until in focus"
      onyxConfig.uploadMetrics = true
      onyxConfig.returnOnyxErrorOnLowQuality = true
      onyxConfig.captureQualityThreshold = 0.7
      onyxConfig.fingerDetectionTimeout = 60
      onyxConfig.onyxCallback = onyxCallback
      onyxConfig.successCallback = onyxSuccessCallback
      onyxConfig.errorCallback = onyxErrorCallback
        
        let onyx: Onyx = Onyx()
        onyx.doSetup(onyxConfig)
    }
    
    func onyxCallback(configuredOnyx: Onyx?) -> Void {
        NSLog("Onyx Callback");
        DispatchQueue.main.async {
            configuredOnyx?.capture(self);
        }
    }
    
    func onyxSuccessCallback(onyxResult: OnyxResult?) -> Void {
        NSLog("Onyx Success Callback");
    }
    
    func onyxErrorCallback(onyxError: OnyxError?) -> Void {
        NSLog("Onyx Error Callback");
        NSLog(onyxError?.errorMessage ?? "Onyx returned an error");
    }
}

